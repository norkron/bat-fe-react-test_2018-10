import reducer from '../index';

describe('bookings reducer', () => {
    test('should return the initial state', () => {
        expect(reducer(undefined, {})).toEqual({
            loading: true,
            reservations: [],
            selected: null
        });
    });

    test('should handle BOOKINGS_FETCH_REQUESTED', () => {
        expect(
            reducer({loading: false}, {type: 'BOOKINGS_FETCH_REQUESTED'})
        ).toEqual({loading: true});
    });

    test('should handle BOOKINGS_FETCH_SUCCEEDED', () => {
        expect(
            reducer(undefined, {type: 'BOOKINGS_FETCH_SUCCEEDED', reservations: [{"lastName": "Bloggs"}]})
        ).toEqual({loading: false, reservations: [{"lastName": "Bloggs"}], selected: null});
    });

    test('should handle BOOKINGS_FETCH_FAILED', () => {
        expect(
            reducer({loading: true}, {type: 'BOOKINGS_FETCH_FAILED'})
        ).toEqual({loading: false});
    });

    test('should handle BOOKINGS_SELECT_FAILED', () => {
        expect(
            reducer(undefined, {type: 'BOOKINGS_SELECT_FAILED', selected: null})
        ).toEqual({
            loading: true,
            reservations: [],
            selected: null
        });
    });

    test('should handle BOOKING_SELECT_SUCCEEDED', () => {
        expect(
            reducer(undefined, {type: 'BOOKINGS_SELECT_FAILED', selected: {"lastName": "Bloggs"}})
        ).toEqual({
            loading: true,
            reservations: [],
            selected: {"lastName": "Bloggs"}
        });
    });
});