export const initialState = {
    loading: true,
    reservations: [],
    selected: null
};

export default (state = initialState, action) => {
    switch (action.type) {
        case 'BOOKINGS_FETCH_REQUESTED':
            return {
                ...state,
                loading: true
            };
        case 'BOOKINGS_FETCH_SUCCEEDED':
            return {
                ...state,
                reservations: action.reservations,
                loading: false
            };
        case 'BOOKINGS_FETCH_FAILED':
            return {
                ...state,
                loading: false
            };
        case 'BOOKINGS_SELECT_FAILED':
        case 'BOOKING_SELECT_SUCCEEDED':
            return {
                ...state,
                selected: action.selected
            };
        case 'BOOKING_UPDATE_SUCCEEDED':
            return {
                ...state,
                selected: action.selected,
                reservations: [{
                    ...state.reservations[0],
                    bookings: action.bookings
                }]
            };
        default:
            return state;
    }
}