import React from 'react';
import {MemoryRouter} from 'react-router';
import renderer from 'react-test-renderer';
import Enzyme, {shallow} from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import {Reservations} from '../Reservations';

Enzyme.configure({adapter: new Adapter()});

describe('ReservationsComponent', () => {
    let reservationsData;
    beforeAll(() => {
        reservationsData = [{
            date: '2017-03-13',
            bookings: [
                {
                    title: 'Mr',
                    firstName: 'Fred',
                    lastName: 'Bloggs',
                    time: '16.00',
                    partySize: 2,
                    seated: false,
                    cancelled: false,
                    notes: ''
                },
                {
                    title: 'Ms',
                    firstName: 'Paula',
                    lastName: 'Higgins',
                    time: '16.30',
                    partySize: 4,
                    seated: true,
                    cancelled: false,
                    notes: ''
                },
                {
                    title: 'Mrs',
                    firstName: 'Annet',
                    lastName: 'Hawthorn',
                    time: '16.30',
                    partySize: 8,
                    seated: false,
                    cancelled: true,
                    notes: ''
                }
            ]
        }];
    });

    test('should tell if data is fetching', () => {
        const reservations = renderer.create(<Reservations reservations={[]} loading={true}/>).toJSON();
        expect(reservations).toMatchSnapshot();
    });

    test('should tell if there are no reservations', () => {
        const reservations = renderer.create(<Reservations reservations={[]} loading={false}/>).toJSON();
        expect(reservations).toMatchSnapshot();
    });

    test('should render one day of bookings', () => {
        const reservations = renderer.create(
            <MemoryRouter>
                <Reservations reservations={reservationsData} loading={false}/>
            </MemoryRouter>
        ).toJSON();

        expect(reservations).toMatchSnapshot();
    });

    test('formatDate should return date in DD/MM/YYY format', () => {
        const formatted = Reservations.formatDate('2017-03-13');
        expect(formatted).toEqual('13/03/2017');
    });

    test('formatDate should return empty string for falsy value', () => {
        const formatted = Reservations.formatDate(undefined);
        expect(formatted).toEqual('');
    });

    test('should display header with date', () => {
        const arrived = shallow(<Reservations reservations={reservationsData} loading={false}/>);
        expect(arrived.find('.bui-reservation__header').first().text()).toEqual('Booking for 13/03/2017');
    });
});
