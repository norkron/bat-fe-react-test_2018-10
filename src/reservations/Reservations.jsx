import React, {PureComponent} from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {Bookings, BookingType} from '../bookings/Bookings';

/**
 * It would be nice to have a datepicker
 * and some arrows on both sides
 * to navigate across different dates.
 * */

const ReservationType = PropTypes.shape({
    date: PropTypes.string.isRequired,
    bookings: PropTypes.arrayOf(BookingType)
});

export class Reservations extends PureComponent {
    static defaultProps = {
        reservations: [],
        fetchReservations: () => {
        }
    };

    static propTypes = {
        reservations: PropTypes.arrayOf(ReservationType).isRequired,
        loading: PropTypes.bool.isRequired,
        fetchReservations: PropTypes.func
    };

    static formatDate(date) {
        if (!date) return '';
        const parsed = new Date(date);
        const month  = parsed.getMonth() + 1;
        return `${parsed.getDate()}/${month > 9 ? month : '0' + month}/${parsed.getFullYear()}`;
    }

    componentDidMount() {
        this.props.fetchReservations();
    }

    render() {
        const {reservations, loading, selectReservation} = this.props;
        if (loading) return <h1>Loading...</h1>;
        return reservations.length > 0 ?
            reservations.map(({date, bookings}) => (
                <div className="bui-reservation" key={date}>
                    <h1 className="bui-reservation__header">
                        Booking for {`${Reservations.formatDate(date)}`}
                    </h1>
                    <Bookings bookings={bookings} selectReservation={selectReservation}/>
                </div>
            ))
            : (<h1>There are no reservations</h1>);
    }
}

export const ReservationsConnected = connect(
    ({reservations, loading}) => ({reservations, loading}),
    (dispatch) => ({
        fetchReservations: () => dispatch({type: 'BOOKINGS_FETCH_REQUESTED'})
    })
)(Reservations);
