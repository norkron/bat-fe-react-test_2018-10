import {fetchBookings} from '../sagas';
import {call, put, takeLatest} from 'redux-saga/effects';
const mockedValue = {
    json: () => [{
        'date': '2017-03-13',
        'bookings': [
            {
                'title': 'Mr',
                'firstName': 'Fred',
                'lastName': 'Bloggs',
                'time': '16.00',
                'partySize': 2,
                'seated': false,
                'cancelled': false,
                'notes': ''
            }
        ]
    }]
};
window.fetch = jest.fn().mockReturnValue(mockedValue);

describe('bookingSaga', () => {
    test('gets data from json, unwraps from promise and dispatches as BOOKINGS_FETCH_SUCCEEDED', () => {
        const gen = fetchBookings();

        const bookings = gen.next();
        expect(bookings).toHaveProperty('done', false);
        expect(window.fetch).toBeCalledWith("../bookings.json");

        const getJson = gen.next(mockedValue);
        expect(getJson).toEqual({done: false, value: mockedValue.json()});

        const put = gen.next(mockedValue.json());
        expect(put).toHaveProperty('value.PUT.action.type', "BOOKINGS_FETCH_SUCCEEDED");
        expect(put).toHaveProperty('value.PUT.action.reservations', mockedValue.json());

        expect(gen.next()).toEqual({done: true});
    });
});