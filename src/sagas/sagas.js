import {all, put, select, take, takeEvery, takeLatest} from 'redux-saga/effects';

export function* fetchBookings() {
    try {
        const bookings     = yield fetch('../bookings.json');
        const reservations = yield bookings && bookings.json();
        yield put({type: 'BOOKINGS_FETCH_SUCCEEDED', reservations: reservations});
    } catch (e) {
        yield put({type: 'BOOKINGS_FETCH_FAILED', message: e.message});
    }
}

export function* selectBooking({type, index}) {
    const reservationsAreEmpty = yield select(({reservations}) => {
        return reservations && reservations.length === 0;
    });
    if (reservationsAreEmpty) yield take('BOOKINGS_FETCH_SUCCEEDED');
    const booking = yield select(state => {
        return state.reservations[0].bookings[index]
    });
    if (booking) {
        yield put({type: 'BOOKING_SELECT_SUCCEEDED', selected: booking});
    } else {
        yield put({type: 'BOOKINGS_SELECT_FAILED', selected: null});
    }
}

export function* updateBooking({type, index, booking}) {
    const bookings = yield select(state => {
        return state.reservations[0].bookings
    });
    const newBookings = [...bookings];
    newBookings.splice(+index, 1, booking);
    if (bookings.length === newBookings.length) {
        yield put({type: 'BOOKING_UPDATE_SUCCEEDED', bookings: newBookings, selected: booking});
    } else {
        yield put({type: 'BOOKINGS_UPDATE_FAILED'});
    }
}

export default function* bookingSaga() {
    yield all([
        takeEvery('BOOKING_SELECT_REQUESTED', selectBooking),
        takeEvery('UPDATE_SELECT_REQUESTED', updateBooking),
        takeLatest('BOOKINGS_FETCH_REQUESTED', fetchBookings)
    ]);
}