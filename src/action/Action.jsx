import React, {Fragment, PureComponent} from 'react';
import {Link} from 'react-router-dom';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {BookingType} from '../bookings/Bookings';
import './Action.css';

export class Action extends PureComponent {
    static propTypes = {
        selected: PropTypes.oneOfType([
            PropTypes.shape(BookingType),
            PropTypes.exact(null),
            PropTypes.exact(undefined)
        ]),
        selectReservation: PropTypes.func,
        updateSelected: PropTypes.func
    };

    static wrapper(children) {
        return (
            <div className="bui-action">
                <h1 className="bui-action__title">Booking update</h1>
                <Link to="/" className="bui-action__close">╳</Link>
                <div className="bui-action__children">
                    {children}
                </div>
            </div>
        );
    }

    componentDidMount() {
        this.props.selectReservation(this.props.match.params.id);
    }

    componentDidUpdate(prevProps) {
        const {match: {params: {id: prevId}}} = prevProps;
        const {match: {params: {id: currId}}} = this.props;
        if (currId !== prevId) {
            this.props.selectReservation(currId);
        }
    }

    selectStatus = (event) => {
        const {value}                                           = event.target;
        const {updateSelected, selected, match: {params: {id}}} = this.props;
        updateSelected(
            id,
            {
                ...selected,
                seated: value !== 'C' && value === 'Y',
                cancelled: value === 'C'
            });
    };

    render() {
        const {selected, match: {params: {id}}} = this.props;
        if ([null, undefined].includes(selected)) {
            return Action.wrapper(
                <h1>Sorry, there's no reservation with ID: {id}</h1>
            );
        }

        const {
            title,
            firstName,
            lastName,
            time,
            partySize,
            seated,
            cancelled,
            notes
        } = selected;

        return Action.wrapper(
            <Fragment>
                <h2>Name</h2>
                <span>{`${title} ${firstName} ${lastName}`}</span>
                <h3>Time</h3>
                <span>{time}</span>
                <h3>Covers</h3>
                <span>{partySize}</span>
                <h3>Seated</h3>
                <label>
                    <input
                        className="bui-status--not-seated"
                        type="radio"
                        name="seated"
                        value="N"
                        checked={!seated && !cancelled}
                        onChange={this.selectStatus}
                    />
                    No
                </label>
                <br/>
                <label>
                    <input
                        className="bui-status--seated"
                        type="radio"
                        name="seated"
                        value="Y"
                        checked={seated}
                        onChange={this.selectStatus}
                    />
                    Yes
                </label>
                <br/>
                <label>
                    <input
                        className="bui-status--cancelled"
                        type="radio"
                        name="seated"
                        value="C"
                        checked={cancelled}
                        onChange={this.selectStatus}
                    />
                    Cancelled
                </label>
                <h3>Notes</h3>
                <span>{notes}</span>
            </Fragment>
        );
    }
}

export const ActionConnected = connect(
    ({selected}) => ({selected}),
    (dispatch) => ({
        selectReservation: (index) => dispatch({type: 'BOOKING_SELECT_REQUESTED', index}),
        updateSelected: (index, booking) => dispatch({type: 'UPDATE_SELECT_REQUESTED', index, booking})
    })
)(Action);
