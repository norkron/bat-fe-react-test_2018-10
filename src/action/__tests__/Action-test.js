import React from 'react';
import renderer from 'react-test-renderer';
import Enzyme, {shallow} from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import {Action} from '../Action';
import {MemoryRouter} from 'react-router';
import {Link} from 'react-router-dom';

Enzyme.configure({adapter: new Adapter()});

describe('ActionComponent', () => {
    let bookingData;
    const selectReservationMock = jest.fn();
    const updateSelectedMock = jest.fn();
    beforeAll(() => {
        bookingData = {
            title: 'Mr',
            firstName: 'Fred',
            lastName: 'Bloggs',
            time: '16.00',
            partySize: 2,
            seated: false,
            cancelled: false,
            notes: 'Lorem ipsum'
        };
    });

    test('renders list of properties', () => {
        const action = renderer.create(
            <MemoryRouter>
                <Action
                    selected={bookingData}
                    match={{params: {id: 2}}}
                    selectReservation={selectReservationMock}
                />
            </MemoryRouter>
        ).toJSON();
        expect(action).toMatchSnapshot();
    });

    describe('.wrapper()', () => {
        let wrappedDiv;
        beforeAll(() => {
            wrappedDiv = shallow(
                <MemoryRouter>
                    {Action.wrapper(<div/>)}
                </MemoryRouter>
            );
        });

        test('should add title', () => {
            expect(wrappedDiv.find('.bui-action__title').text()).toEqual('Booking update');
        });

        test('should add close button with Link to /', () => {
            const button = wrappedDiv.find('.bui-action__close').getElement();
            expect(button.type).toEqual(Link);
            expect(button.props.to).toEqual('/');
        });

        test('should pass children', () => {
            expect(wrappedDiv.find('.bui-action__children').childAt(0).html()).toEqual('<div></div>');
        });
    });

    test('should tell if there is no such reservation at index x', () => {
        const noReservation = shallow(
            <Action
                selected={null}
                match={{params: {id: 2}}}
                selectReservation={selectReservationMock}
            />
        );

        expect(noReservation.find('.bui-action__children h1').text())
            .toEqual('Sorry, there\'s no reservation with ID: 2');
    });

    test('should enable user to change status of reservation', () => {
        const changeStatus = shallow(
            <Action
                selected={bookingData}
                match={{params: {id: 2}}}
                selectReservation={selectReservationMock}
                updateSelected={updateSelectedMock}
            />
        );
        const seated = changeStatus.find('.bui-status--seated');
        const seatedVal = seated.getElement().props.value;
        seated.simulate('change', {target: {value: seatedVal}});

        expect(updateSelectedMock).toBeCalledWith(2, {
            ...bookingData,
            seated: true
        });

        const notSeated = changeStatus.find('.bui-status--not-seated');
        const notSeatedVal = notSeated.getElement().props.value;
        notSeated.simulate('change', {target: {value: notSeatedVal}});

        expect(updateSelectedMock).toBeCalledWith(2, {
            ...bookingData,
            seated: false
        });

        const cancelled = changeStatus.find('.bui-status--cancelled');
        const cancelledVal = cancelled.getElement().props.value;
        cancelled.simulate('change', {target: {value: cancelledVal}});

        expect(updateSelectedMock).toBeCalledWith(2, {
            ...bookingData,
            seated: false,
            cancelled: true
        });
    });
});
