import React from 'react';
import renderer from 'react-test-renderer';
import Enzyme, {shallow} from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import {Bookings} from '../Bookings';
import {MemoryRouter} from 'react-router';

Enzyme.configure({adapter: new Adapter()});

describe('BookingsComponent', () => {
    let bookingsData;
    beforeAll(() => {
        bookingsData = [
            {
                title: 'Mr',
                firstName: 'Fred',
                lastName: 'Bloggs',
                time: '16.00',
                partySize: 2,
                seated: false,
                cancelled: false,
                notes: ''
            },
            {
                title: 'Ms',
                firstName: 'Paula',
                lastName: 'Higgins',
                time: '16.30',
                partySize: 4,
                seated: true,
                cancelled: false,
                notes: ''
            },
            {
                title: 'Mrs',
                firstName: 'Annet',
                lastName: 'Hawthorn',
                time: '16.30',
                partySize: 8,
                seated: false,
                cancelled: true,
                notes: ''
            }
        ];
    });

    test('renders table with three different records', () => {
        const bookings = renderer.create(
            <MemoryRouter>
                <Bookings bookings={bookingsData}/>
            </MemoryRouter>
        ).toJSON();
        expect(bookings).toMatchSnapshot();
    });

    test('should tell one booking from another', () => {
        const expectedNames = ['Mr Fred Bloggs', 'Ms Paula Higgins', 'Mrs Annet Hawthorn'];
        const bookings      = shallow(
            <MemoryRouter>
                <Bookings bookings={bookingsData}/>
            </MemoryRouter>
        );

        const rows   = bookings.find('.bui-booking__row');
        const length = rows.length;

        for (let i = 0; i < length; i++) {
            expect(rows.at(i).childAt(0).text()).toEqual(expectedNames[i]);
        }
    });

    test('should tell which diners have already arrived', () => {
        const arrived = shallow(
            <Bookings
                bookings={[
                    {
                        title: 'Mr',
                        firstName: 'Fred',
                        lastName: 'Bloggs',
                        time: '16.00',
                        partySize: 2,
                        seated: true,
                        cancelled: false,
                        notes: ''
                    }
                ]}
            />
        );
        expect(arrived.find('.bui-booking__cell').last().text()).toEqual('Y');
    });

    test('should tell which diners have yet to arrive', () => {
        const notArrived = shallow(
            <Bookings
                bookings={[
                    {
                        title: 'Mr',
                        firstName: 'Fred',
                        lastName: 'Bloggs',
                        time: '16.00',
                        partySize: 2,
                        seated: false,
                        cancelled: false,
                        notes: ''
                    }
                ]}
            />
        );
        expect(notArrived.find('.bui-booking__cell').last().text()).toEqual('N');
    });

    test('should tell which booking to expect to arrive next', () => {
    });

    test('should tell which diners have cancelled their bookings', () => {
        const arrived = shallow(
            <Bookings
                bookings={[
                    {
                        title: 'Mr',
                        firstName: 'Fred',
                        lastName: 'Bloggs',
                        time: '16.00',
                        partySize: 2,
                        seated: false,
                        cancelled: true,
                        notes: ''
                    }
                ]}
            />
        );
        expect(arrived.find('.bui-booking__row').last().hasClass('bui-booking__row--cancelled')).toBeTruthy();
    });
});
