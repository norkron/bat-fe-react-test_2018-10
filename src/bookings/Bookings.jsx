import React, {PureComponent, Fragment} from 'react';
import {Link} from 'react-router-dom';
import PropTypes from 'prop-types';
import './Bookings.css';

export const BookingType = PropTypes.shape({
    title: PropTypes.string,
    firstName: PropTypes.string.isRequired,
    lastName: PropTypes.string.isRequired,
    time: PropTypes.string.isRequired,
    partySize: PropTypes.number.isRequired,
    seated: PropTypes.bool,
    cancelled: PropTypes.bool,
    notes: PropTypes.string
});

export class Bookings extends PureComponent {
    static defaultProps = {
        bookings: []
    };

    static propTypes = {
        bookings: PropTypes.arrayOf(BookingType)
    };

    render() {
        const {bookings} = this.props;
        return (
            <Fragment>
                <div className="bui-booking__table">
                    <div className="bui-booking__row bui-booking__row--header">
                        <div className="bui-booking__cell bui-booking__cell--header bui-booking__cell--main">Name</div>
                        <div className="bui-booking__cell bui-booking__cell--header">Time</div>
                        <div className="bui-booking__cell bui-booking__cell--header">Covers</div>
                        <div className="bui-booking__cell bui-booking__cell--header">Seated</div>
                    </div>
                    <div className="bui-booking__body">
                        {bookings && bookings.map((b, idx) => {
                            return (
                                <Link
                                    to={'/reservation/' + idx}
                                    className={`bui-booking__row ${b.cancelled ? 'bui-booking__row--cancelled' : ''}`}
                                    key={b.firstName + b.lastName + b.time}
                                >
                                    {Object.entries({
                                        name: `${b.title} ${b.firstName} ${b.lastName}`,
                                        time: b.time,
                                        covers: b.partySize,
                                        seated: b.seated ? 'Y' : 'N'
                                    }).map(([key, value]) => (
                                        <div
                                            className={
                                                `bui-booking__cell ${key === 'name' ? 'bui-booking__cell--main' : ''}`
                                            }
                                            key={key + value}
                                        >
                                            {value}
                                        </div>
                                    ))}
                                </Link>
                            );
                        })}
                    </div>
                </div>
            </Fragment>
        );
    }
}