/* Feel free to edit */
import React, {Component, Fragment} from 'react';
import {Provider} from 'react-redux';

import {Route} from 'react-router';
import {BrowserRouter} from 'react-router-dom';
import {createStore, applyMiddleware} from 'redux';
import createSagaMiddleware from 'redux-saga';

import {ActionConnected} from './action/Action';
import {ReservationsConnected} from './reservations/Reservations';
import bookatableLogo from './assets/bookatable-by-michelin-logo.svg';
import './App.css';

import reducer from './reducers';
import rootSaga from './sagas/sagas';

const sagaMiddleware = createSagaMiddleware();
const store          = createStore(reducer, applyMiddleware(sagaMiddleware));
sagaMiddleware.run(rootSaga);

if (module.hot) {
    module.hot.accept('./reducers', () => {
        const newReducer = require('./reducers/index');
        store.replaceReducer(newReducer);
    });
}

export default class App extends Component {
    render() {
        return (
            <div className="bui-app">
                <header className="bui-header">
                    <img
                        alt="Bookatable❤️ by Michelin"
                        className="bui-header__logo"
                        height="35"
                        src={bookatableLogo}
                        width="120"
                    />
                </header>
                <Provider store={store}>
                    <BrowserRouter>
                        <Fragment>
                            <section className="bui-main">
                                <Route path="/" component={ReservationsConnected}/>
                            </section>
                            <Route path="/reservation/:id" render={props => (
                                <aside className="bui-aside">
                                    <ActionConnected {...props}/>
                                </aside>
                            )}/>
                        </Fragment>
                    </BrowserRouter>
                </Provider>
            </div>
        );
    }
}
